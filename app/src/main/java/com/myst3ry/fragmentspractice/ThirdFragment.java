package com.myst3ry.fragmentspractice;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public final class ThirdFragment extends Fragment {

    public static final String TAG = ThirdFragment.class.getSimpleName();
    private static final String EXTRA_TEXT = BuildConfig.APPLICATION_ID + "extra.TEXT";

    public static Fragment newInstance(final String text) {
        final ThirdFragment fragment = new ThirdFragment();
        final Bundle args = new Bundle();
        args.putString(EXTRA_TEXT, text);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_third, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final TextView textView = view.findViewById(R.id.text_view);
        textView.setText(getArguments() != null ? getArguments().getString(EXTRA_TEXT) : "");
    }
}
