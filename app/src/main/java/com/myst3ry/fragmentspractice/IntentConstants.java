package com.myst3ry.fragmentspractice;

public final class IntentConstants {

    public static final String ACTION_SEND_MILLIS = BuildConfig.APPLICATION_ID + "action.SEND_MILLIS";
    public static final String EXTRA_STATE = BuildConfig.APPLICATION_ID + "extra.STATE";

    private IntentConstants() {
    }
}
