package com.myst3ry.fragmentspractice;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

public final class SecondFragment extends Fragment {

    private Button btnStart;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setListeners();
    }

    private void initViews(@NonNull final View view) {
        btnStart = view.findViewById(R.id.button);
    }

    private void setListeners() {
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getChildFragmentManager().beginTransaction()
                        .replace(R.id.container, ThirdFragment.newInstance(getText()), ThirdFragment.TAG)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            }
        });
    }

    private String getText() {
        try {
            return ((MainActivity) Objects.requireNonNull(getActivity())).getText();
        } catch (ClassCastException e) {
            throw new ClassCastException(getString(R.string.class_cast_exp_text));
        }
    }
}
