package com.myst3ry.fragmentspractice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public final class MyService extends Service {

    private static final int TIMER_PERIOD = 1000;
    private static final int TIME_DELAY = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                final String time = Objects.toString(System.currentTimeMillis());
                final Intent intent = new Intent();
                intent.setAction(IntentConstants.ACTION_SEND_MILLIS);
                intent.putExtra(IntentConstants.EXTRA_STATE, time);
                intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                sendBroadcast(intent);
            }
        }, TIME_DELAY, TIMER_PERIOD);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static Intent newIntent(Context context) {
        return new Intent(context, MyService.class);
    }
}
