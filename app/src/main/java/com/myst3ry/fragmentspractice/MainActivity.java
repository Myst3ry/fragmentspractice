package com.myst3ry.fragmentspractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public final class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(MyService.newIntent(this));
    }


    public String getText() {
        final FirstFragment firstFragment = (FirstFragment) getSupportFragmentManager().findFragmentById(R.id.first_fragment);
        return firstFragment.getTextFromField();
    }
}
