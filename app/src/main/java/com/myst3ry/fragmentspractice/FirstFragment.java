package com.myst3ry.fragmentspractice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.Objects;

public final class FirstFragment extends Fragment {

    private BroadcastReceiver receiver;
    private IntentFilter filter;
    private EditText editText;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editText = view.findViewById(R.id.edit_text);
        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        Objects.requireNonNull(getActivity()).registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        Objects.requireNonNull(getActivity()).unregisterReceiver(receiver);
    }

    private void init() {
        receiver = new TimeBroadCastReceiver();
        filter = new IntentFilter(IntentConstants.ACTION_SEND_MILLIS);
    }

    public String getTextFromField() {
        return editText.getText().toString();
    }


    private final class TimeBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            editText.setText(intent.getStringExtra(IntentConstants.EXTRA_STATE));
        }
    }
}
